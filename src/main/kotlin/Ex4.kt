fun main (){
    val equiposDeLaLigaClasificados = mutableListOf("Barça", "Real Madrid", "Atletico de Madrid", "Real Sociedad")

    try {
        equiposDeLaLigaClasificados.add("Osasuna")
    }catch (e:NoSuchElementException){
        println("Error: NoSuchElementException")
    }

    try {
        equiposDeLaLigaClasificados.remove("Real Madrid")
        println(equiposDeLaLigaClasificados)
    } catch (e: UnsupportedOperationException) {
        println("Error: UnsupportedOperationException")
    }

}