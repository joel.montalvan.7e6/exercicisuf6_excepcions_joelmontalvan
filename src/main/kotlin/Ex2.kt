import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introduce un numero:")
    val parametro = scanner.next()

    println(aDoubleoU(parametro))

}

fun aDoubleoU(parametro:String):Double{

    val resultadoFinal = try {
        parametro.toDouble()
    } catch (e: NumberFormatException){
        1.0
    }
    return resultadoFinal
}