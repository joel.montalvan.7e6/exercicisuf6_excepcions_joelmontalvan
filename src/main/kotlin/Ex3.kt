import java.io.File
import java.io.IOException

fun main(){
    val archivo = File("archivoInexistente.txt")
    try {
        val lecturaArchivo = archivo.readText()
        println(lecturaArchivo)
    }catch (_: IOException){
        println("$archivo (El sistema no puede encontrar el archivo especificado)")
    }
}