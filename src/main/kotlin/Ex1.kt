import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introduce un dividiendo y un divisor:")
    val dividiendo = scanner.nextInt()
    val divisor = scanner.nextInt()

    println(dividiendoPorCero(dividiendo, divisor))

}

fun dividiendoPorCero(dividiendo:Int, divisor:Int):Int{
    var resultadoFinal = 0
    try {
        resultadoFinal = dividiendo/divisor
    } catch (e: ArithmeticException){
        println("Error: Estas haciendo una división por cero!!!")
    }
    return resultadoFinal
}